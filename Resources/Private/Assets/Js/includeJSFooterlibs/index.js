/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

var tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map = [];

var tx_hivecptcntgooglemap_hivecptcntgooglemaprender__httpClient = function () {
  this.get = function (aUrl, aCallback) {
    var anHttpRequest = new XMLHttpRequest();
    anHttpRequest.onreadystatechange = function () {
      if (anHttpRequest.readyState == 4) {
        if (anHttpRequest.status == 200) {
          aCallback(anHttpRequest.responseText);
        } else {
          aCallback(null);
        }
      }

    };

    anHttpRequest.open("GET", aUrl, true);
    anHttpRequest.send(null);
  }
};

function tx_hivecptcntgooglemap_hivecptcntgooglemaprender__loadScript(url, callback) {

  var script = document.createElement("script");
  script.type = "text/javascript";
  script.async = true;

  if (script.readyState) { //IE
    script.onreadystatechange = function () {
      if (script.readyState == "loaded" || script.readyState == "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else { //Others
    script.onload = function () {
      callback();
    };
  }

  script.src = url;
  //document.getElementsByTagName("head")[0].appendChild(script);

  // fetch our section element
  var head_ = document.querySelector("head");

  // prepend our script eleemnt to our head element
  head_.insertBefore(script, head_.firstChild);
}

var tx_hivecptcntgooglemap_hivecptcntgooglemaprender__interval = setInterval(function () {
  if (typeof hive_cfg_typoscript__windowLoad == 'undefined') {
  } else {
    if (typeof hive_cfg_typoscript__windowLoad == "boolean" && hive_cfg_typoscript__windowLoad) {

      clearInterval(tx_hivecptcntgooglemap_hivecptcntgooglemaprender__interval);

      /**
       *
       * @type {NodeList}
       */
      var oMap = document.getElementsByClassName("hivecptcntgooglemap");
      if (typeof oMap == "object" && oMap.length > 0) {

        /**
         * load google api
         */
        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__loadScript("https://www.google.com/jsapi", function () {

          if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
            console.info("tx_hivecptcntgooglemap :: Google API :: ready");
          }

          google.load("maps", "3", {
            other_params: 'key=AIzaSyAVLdOymShFyRl329pcdpVxf0R0lNytiWM',
            callback: function () {
              var google_maps__interval = setInterval(function () {
                if (typeof google == 'undefined') {
                } else {
                  if (typeof google == 'undefined' && typeof google.maps == 'undefined' && typeof mapIcons == 'undefined') {
                  } else {
                    clearInterval(google_maps__interval);
                    if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                      console.info("tx_hivecptcntgooglemap :: google and google.maps :: ready");
                    }

                    for (var i = 0; i < oMap.length; i++) {
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["state"] = "undefined";

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"] = [];

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["status"] = "undefined";
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["responseText"] = {"zoom": 8};

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["status"] = "undefined";
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["responseText"] = [];

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"]["status"] = "undefined";
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"]["responseText"] = [];

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["status"] = "undefined";
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"] = "undefined";

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"] = {};
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["markerBounds"] = {};
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["infoWindows"] = [];

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sMapId"] = oMap[i].getAttribute('id');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sPathOptions"] = oMap[i].getAttribute('data-options');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sPathStyles"] = oMap[i].getAttribute('data-styles');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiPid"] = oMap[i].getAttribute('data-api-pid');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiToken"] = oMap[i].getAttribute('data-api-token');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiType"] = oMap[i].getAttribute('data-api-type');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiMapId"] = oMap[i].getAttribute('data-api-map-id');

                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"] = [];
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sTableDocId"] = oMap[i].getAttribute('data-fusion-docid');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sTabelSelectField"] = oMap[i].getAttribute('data-fusion-tabelselectfield');
                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sPathStyles"] = oMap[i].getAttribute('data-fusion-styles');
                    }

                    /**
                     * Get Options and Styles
                     */

                    var o = (function () {
                      var xhr = [];
                      for (i = 0; i < tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map.length; i++) {
                        (function (i) {
                          xhr[i] = new XMLHttpRequest();
                          url = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sPathOptions"];
                          xhr[i].open("GET", url, true);
                          xhr[i].onreadystatechange = function () {
                            if (xhr[i].readyState == 4) {
                              if (xhr[i].status == 200) {
                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["status"] = xhr[i].status;
                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["responseText"] = JSON.parse(xhr[i].responseText);
                              } else {
                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["status"] = xhr[i].status;
                              }

                              /**
                               * @type {string}
                               */
                              tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["state"] = "options";
                            }
                          };
                          xhr[i].send();
                        })(i);
                      }
                    })();

                    var s = (function () {
                      var xhr = [];
                      for (i = 0; i < tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map.length; i++) {
                        (function (i) {
                          xhr[i] = new XMLHttpRequest();
                          url = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sPathStyles"];
                          xhr[i].open("GET", url, true);
                          xhr[i].onreadystatechange = function () {
                            if (xhr[i].readyState == 4) {
                              if (xhr[i].status == 200) {
                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["status"] = xhr[i].status;
                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["responseText"] = JSON.parse(xhr[i].responseText);
                              } else {
                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["status"] = xhr[i].status;
                              }

                              /**
                               * @type {string}
                               */
                              tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["state"] = "styles";

                            }
                          };
                          xhr[i].send();
                        })(i);
                      }
                    })();

                      var fs = (function () {
                          var xhr = [];
                          for (i = 0; i < tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map.length; i++) {
                              (function (i) {
                                  if (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sPathStyles"]) {
                                    xhr[i] = new XMLHttpRequest();
                                    url = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sPathStyles"];
                                    xhr[i].open("GET", url, true);
                                    xhr[i].onreadystatechange = function () {
                                        if (xhr[i].readyState == 4) {
                                            if (xhr[i].status == 200) {
                                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"]["status"] = xhr[i].status;
                                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"]["responseText"] = JSON.parse(xhr[i].responseText);
                                            } else {
                                                tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"]["status"] = xhr[i].status;
                                            }

                                            /**
                                             * @type {string}
                                             */
                                            tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["state"] = "fusionStyles";
                                        }
                                    };
                                    xhr[i].send();
                                  }
                              })(i);
                          }
                      })();


                    /**
                     * Get Map with coordinates after options and styles loaded successfully
                     */

                    /**
                     *
                     * @type {boolean}
                     */
                    var os = false;

                    /**
                     *
                     * @type {number}
                     */
                    var mc__interval = setInterval(function () {

                      for (i = 0; i < tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map.length; i++) {
                        if (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["status"] == "undefined"
                          || tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["status"] == "undefined") {
                        } else {
                          os = true;
                        }
                      }

                      if (os) {
                        clearInterval(mc__interval);
                        if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                          console.info("tx_hivecptcntgooglemap :: map options and styles :: ready");
                        }
                        var mc = (function () {
                          var xhr = [];
                          for (i = 0; i < tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map.length; i++) {

                            /**
                             * Get Map Coordinates from API
                             */
                            (function (i) {
                              xhr[i] = new XMLHttpRequest();

                              var sApiPid = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiPid"];
                              var sApiToken = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiToken"];
                              var sApiType = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiType"];
                              var sApiMapId = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sApiMapId"];
                              var sPathMap = '/index.php?id=' + sApiPid + '&token=' + sApiToken + '&type=' + sApiType +
                                '&ext=hive_cpt_cnt_google_map' +
                                '&tx_hivecptcntgooglemap_hivecptcntgooglemaprequest[map-api]=' + sApiMapId;

                              url = sPathMap;
                              xhr[i].open("GET", url, true);
                              xhr[i].onreadystatechange = function () {
                                if (xhr[i].readyState == 4) {
                                  if (xhr[i].status == 200) {
                                    tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["status"] = xhr[i].status;
                                    tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"] =
                                      JSON.parse(xhr[i].responseText);

                                    /**
                                     * Render empty map
                                     */
                                    var sMapId = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["sAttributes"]["sMapId"];
                                    var oOptions = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oOptions"]["responseText"];
                                    oOptions.styles = tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oStyles"]["responseText"];
                                    oOptions.center =
                                      new google.maps.LatLng
                                      (
                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0].centerCoordinate.lat,
                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0].centerCoordinate.lon
                                      );
                                    tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"] =
                                      new google.maps.Map(document.getElementById(sMapId), oOptions);

                                    var oCoordinates =
                                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0].coordinate;
                                    var aMarkerOptions = [];
                                    if (typeof oCoordinates == "object" && oCoordinates.length > 0) {

                                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["markerBounds"] =
                                        new google.maps.LatLngBounds();

                                      var xhr_infoWindow = [];
                                      xhr_infoWindow[i] = [];

                                      var xhr_customMarkerIcon = [];
                                      xhr_customMarkerIcon[i] = [];
                                      for (var ii = 0; ii < oCoordinates.length; ii++) {

                                        var ICON = {};

                                        if (typeof tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["icon"] == "object"
                                        && tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["icon"] != null) {

                                          var x = 0, y = 0,
                                            one = parseInt(tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["iconSize"]),
                                            oneHalf = parseInt(one/2);
                                          switch (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["iconAnchor"]) {
                                            case 'bottom left':
                                              y = one;
                                              break;
                                            case 'bottom center':
                                              x = oneHalf;
                                              y = one;
                                              break;
                                            case 'bottom right':
                                              x = one;
                                              y = one;
                                              break;
                                            case 'center right':
                                              x = one;
                                              y = oneHalf;
                                              break;
                                            case 'top right':
                                              x = one;
                                              break;
                                            case 'top center':
                                              x = oneHalf;
                                              break;
                                            case 'top left':
                                              break;
                                            case 'center left':
                                              y = oneHalf;
                                              break;
                                            case 'center center':
                                                x = oneHalf;
                                                y = oneHalf;
                                                break;
                                          }

                                          ICON = {
                                            url: tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["icon"]["originalResource"]["publicUrl"],
                                            // This marker is 20 pixels wide by 32 pixels high.
                                            size: new google.maps.Size(one, one),
                                            // The origin for this image is (0, 0).
                                            origin: new google.maps.Point(0, 0),
                                            // The anchor for this image is the base of the flagpole at (0, 32).
                                            anchor: new google.maps.Point(x, y)
                                          };


                                        } else {
                                          var PATH = "";
                                          switch (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["markerIcon"]) {
                                            case "MAP_PIN":
                                              PATH = mapIcons.shapes.MAP_PIN;
                                              break;
                                            case "SQUARE_PIN":
                                              PATH = mapIcons.shapes.SQUARE_PIN;
                                              break;
                                            case "SHIELD":
                                              PATH = mapIcons.shapes.SHIELD;
                                              break;
                                            case "ROUTE":
                                              PATH = mapIcons.shapes.ROUTE;
                                              break;
                                            case "SQUARE":
                                              PATH = mapIcons.shapes.SQUARE;
                                              break;
                                            case "SQUARE_ROUNDED":
                                              PATH = mapIcons.shapes.SQUARE_ROUNDED;
                                              break;
                                            default:
                                              PATH = mapIcons.shapes.MAP_PIN;
                                          }

                                          ICON = {
                                            path: PATH,
                                            fillColor: (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["hex"] == "" ?
                                              "#000" : tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"][0]["markerIcon"]["hex"]),
                                            fillOpacity: 1,
                                            strokeColor: '',
                                            strokeWeight: 0
                                          };
                                        }

                                        aMarkerOptions = {
                                          "position": {
                                            lat: oCoordinates[ii]["lat"],
                                            lng: oCoordinates[ii]["lon"]
                                          },
                                          // animation: google.maps.Animation.DROP,
                                          icon: ICON
                                        };

                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii] =
                                          new google.maps.Marker(aMarkerOptions);


                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii]
                                          .setMap(tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"]);

                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["markerBounds"]
                                          .extend({lat: oCoordinates[ii]["lat"], lng: oCoordinates[ii]["lon"]});

                                        /**
                                         * Custom MarkerIcon for Coordinate
                                         */
                                        (function (i, ii, xhr, markerIcon) {
                                        xhr[ii] = new XMLHttpRequest();
                                        var sPathMarkericon = '/index.php?id=' + sApiPid + '&token=' + sApiToken + '&type=' + sApiType +
                                          '&ext=hive_cpt_cnt_google_map' +
                                          '&tx_hivecptcntgooglemap_hivecptcntgooglemaprequest[marker_icon-api]=' +
                                          '&findby[coordinate]=' + oCoordinates[ii]["uid"];
                                        url = sPathMarkericon;
                                        xhr[ii].open("GET", url, true);
                                        xhr[ii].onreadystatechange = function () {
                                          if (xhr[ii].readyState == 4) {
                                            if (xhr[ii].status == 200) {
                                              var oMarkerIcon = JSON.parse(xhr[ii].responseText);
                                              if (typeof oMarkerIcon == "object" && oMarkerIcon.length > 0) {

                                                if (typeof oMarkerIcon[0]["icon"] == "object" && oMarkerIcon[0]["icon"] != null) {

                                                  var x = 0, y = 0, oneHalf = parseInt(parseInt(oMarkerIcon[0]["iconSize"]) / 2);
                                                  switch (oMarkerIcon[0]["iconAnchor"]) {
                                                    case 'bottom left':
                                                      y = parseInt(oMarkerIcon[0]["iconSize"]);
                                                      break;
                                                    case 'bottom center':
                                                      x = oneHalf;
                                                      y = parseInt(oMarkerIcon[0]["iconSize"]);
                                                      break;
                                                    case 'bottom right':
                                                      x = parseInt(oMarkerIcon[0]["iconSize"]);
                                                      y = parseInt(oMarkerIcon[0]["iconSize"]);
                                                      break;
                                                    case 'center right':
                                                      x = parseInt(oMarkerIcon[0]["iconSize"]);
                                                      y = oneHalf;
                                                      break;
                                                    case 'top right':
                                                      x = parseInt(oMarkerIcon[0]["iconSize"]);
                                                      break;
                                                    case 'top center':
                                                      x = oneHalf;
                                                      break;
                                                    case 'top left':
                                                      break;
                                                    case 'center left':
                                                      y = oneHalf;
                                                      break;
                                                    case 'center center':
                                                        x = oneHalf;
                                                        y = oneHalf;
                                                        break;
                                                  }

                                                  var image = {
                                                    url: oMarkerIcon[0]["icon"]["originalResource"]["publicUrl"],
                                                    // This marker is 20 pixels wide by 32 pixels high.
                                                    size: new google.maps.Size(parseInt(oMarkerIcon[0]["iconSize"]), parseInt(oMarkerIcon[0]["iconSize"])),
                                                    // The origin for this image is (0, 0).
                                                    origin: new google.maps.Point(0, 0),
                                                    // The anchor for this image is the base of the flagpole at (0, 32).
                                                    anchor: new google.maps.Point(x, y)
                                                  };
                                                  tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii].setIcon(image);
                                                  // tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii].MarkerLabel.div.innerHTML =
                                                  //   '<img src="/' + oMarkerIcon[0]["icon"]["originalResource"]["publicUrl"] + '">';
                                                } else {
                                                  var PATH_ = "";
                                                  switch (oMarkerIcon[0]["markerIcon"]) {
                                                    case "MAP_PIN":
                                                      PATH_ = mapIcons.shapes.MAP_PIN;
                                                      break;
                                                    case "SQUARE_PIN":
                                                      PATH_ = mapIcons.shapes.SQUARE_PIN;
                                                      break;
                                                    case "SHIELD":
                                                      PATH_ = mapIcons.shapes.SHIELD;
                                                      break;
                                                    case "ROUTE":
                                                      PATH_ = mapIcons.shapes.ROUTE;
                                                      break;
                                                    case "SQUARE":
                                                      PATH_ = mapIcons.shapes.SQUARE;
                                                      break;
                                                    case "SQUARE_ROUNDED":
                                                      PATH_ = mapIcons.shapes.SQUARE_ROUNDED;
                                                      break;
                                                    default:
                                                      PATH_ = mapIcons.shapes.MAP_PIN;
                                                  }

                                                  var ICON_ = {
                                                    path: PATH_,
                                                    fillOpacity: 1,
                                                    strokeColor: '',
                                                    strokeWeight: 0,
                                                  };

                                                  if (oMarkerIcon[0]["hex"] != "") {
                                                    ICON_.fillColor = oMarkerIcon[0]["hex"];
                                                  }

                                                  tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii].setIcon(ICON_);
                                                }

                                              }
                                            } else {

                                            }
                                          }
                                        };
                                        xhr[ii].send();
                                      })(i, ii, xhr_customMarkerIcon, ICON);



                                        /**
                                         * InfoWindow
                                         */
                                        (function (i, ii, xhr) {
                                          xhr[ii] = new XMLHttpRequest();
                                          var sPathInfoWindow = '/index.php?id=' + sApiPid + '&token=' + sApiToken + '&type=' + sApiType +
                                            '&ext=hive_cpt_cnt_google_map' +
                                            '&tx_hivecptcntgooglemap_hivecptcntgooglemaprequest[info_window-api]=' +
                                            '&findby[coordinate]=' + oCoordinates[ii]["uid"];
                                          url = sPathInfoWindow;
                                          xhr[ii].open("GET", url, true);
                                          xhr[ii].onreadystatechange = function () {
                                            if (xhr[ii].readyState == 4) {
                                              if (xhr[ii].status == 200) {
                                                 var oInfoWindow = JSON.parse(xhr[ii].responseText);
                                                if (typeof oInfoWindow == "object" && oInfoWindow.length > 0) {
                                                  if (typeof oInfoWindow[0] == "object" && oInfoWindow[0] != null) {
                                                    var sContent = '' +
                                                      '<div class="card">' +
                                                        '###IMG###' +
                                                        '<div class="card-body">' +
                                                          oInfoWindow[0]["text"] +
                                                        '</div>' +
                                                      '</div>';
                                                    if (typeof oInfoWindow[0]["image"] == "object" && oInfoWindow[0]["image"] != null) {
                                                      var sImgSrc = oInfoWindow[0]["image"]["originalResource"]["publicUrl"];
                                                      var sImg = '<img class="card-img-top" src="' + sImgSrc + '">';
                                                      sContent = sContent.replace("###IMG###", sImg);
                                                    } else {
                                                      sContent = sContent.replace("###IMG###", "");
                                                    }
                                                    tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["infoWindows"][ii] =
                                                    new google.maps.InfoWindow({
                                                      content: sContent
                                                    });
                                                    /**
                                                     * Listener for InfoWindow
                                                     */
                                                    google.maps.event.addListener(
                                                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii],
                                                      'click', (function (map, marker, infoWindow) {
                                                        return function () {
                                                          infoWindow.open(map, marker);
                                                        }
                                                      })(
                                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"],
                                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii],
                                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["infoWindows"][ii]
                                                      ));
                                                  }
                                                }
                                        //           var sContent = '';
                                        //           for (var a = 0; a < oAddress.length; a++) {
                                        //             sContent +=
                                        //               '<address>';
                                        //
                                        //             sContent +=
                                        //               '<h3>' +
                                        //               oAddress[a]["title"] +
                                        //               '</h3>' +
                                        //               oAddress[a]["street"] +
                                        //               ' ' +
                                        //               oAddress[a]["nr"];
                                        //
                                        //             if (typeof oAddress[a]['zip'] == "object" && oAddress[a]['zip'] != null &&
                                        //               typeof oAddress[a]['city'] == "object" && oAddress[a]['city'] != null) {
                                        //
                                        //               sContent +=
                                        //                 '<br>';
                                        //
                                        //               if (typeof oAddress[a]['city']['stateProvince'] == "object"
                                        //                 && oAddress[a]['city']['stateProvince'] != null) {
                                        //                 if (typeof oAddress[a]['city']['stateProvince']['country'] == "object"
                                        //                   && oAddress[a]['city']['stateProvince']['country'] != null) {
                                        //                   sContent +=
                                        //                     oAddress[a]['city']['stateProvince']['country'].iso3166Alpha2 +
                                        //                     ' - ';
                                        //                 }
                                        //
                                        //               }
                                        //
                                        //                 sContent +=
                                        //                 oAddress[a]['zip'].title +
                                        //                 ' ' +
                                        //                 oAddress[a]['city'].title;
                                        //
                                        //               if (typeof oAddress[a]['city']['stateProvince'] == "object"
                                        //                 && oAddress[a]['city']['stateProvince'] != null) {
                                        //                 if (typeof oAddress[a]['city']['stateProvince']['country'] == "object"
                                        //                   && oAddress[a]['city']['stateProvince']['country'] != null) {
                                        //                   sContent +=
                                        //                     ', ' +
                                        //                     oAddress[a]['city']['stateProvince'].title;
                                        //                 }
                                        //
                                        //               }
                                        //
                                        //             }
                                        //
                                        //             sContent +=
                                        //               '</address>';
                                        //           }
                                        //           if (hive_cfg_typoscript_sStage == "prototype" ||
                                        //             hive_cfg_typoscript_sStage == "development") {
                                        //             console.info('sContent: ' + sContent);
                                        //           }
                                        //
                                        //           if (sContent != '') {
                                        //             tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["infoWindows"][ii] =
                                        //               new google.maps.InfoWindow({
                                        //                 content: sContent
                                        //               });
                                        //
                                        //             /**
                                        //              * Listener for InfoWindow
                                        //              */
                                        //             google.maps.event.addListener(
                                        //               tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii],
                                        //               'click', (function (map, marker, infoWindow) {
                                        //                 return function () {
                                        //                   infoWindow.open(map, marker);
                                        //                 }
                                        //               })(
                                        //                 tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"],
                                        //                 tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["marker"][ii],
                                        //                 tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["infoWindows"][ii]
                                        //               ));
                                        //           }
                                        //
                                        //
                                        //         }
                                              } else {

                                              }
                                            }
                                          };
                                          xhr[ii].send();
                                        })(i, ii, xhr_infoWindow);
                                      }

                                      /**
                                       * @type {string}
                                       */
                                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["state"] = "marker";

                                      /**
                                       * Fit if more than one marker
                                       */
                                      if (oCoordinates.length > 1) {
                                        tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"].fitBounds(
                                          tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["markerBounds"]
                                        );
                                      }
                                    } else {
                                      /**
                                       * @type {string}
                                       */
                                      tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["state"] = "nomarker";
                                    }

                                      /**
                                       *  Add fusionTablesLayer
                                       */
                                      if (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sTableDocId"] && tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sTabelSelectField"]) {
                                          var layer = new google.maps.FusionTablesLayer({
                                              suppressInfoWindows: true,
                                              map: tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMap"]["map"],
                                              query: {
                                                  select: tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sTabelSelectField"],
                                                  from: tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["fusionTablesLayer"]["sTableDocId"]
                                              },
                                              styles: tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oFusionStyles"]["responseText"]
                                          });
                                      }

                                  } else {
                                    tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["status"] = xhr[i].status;
                                    tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[i]["oMapAndCoordinates"]["responseText"] = [];
                                  }
                                }
                              };
                              xhr[i].send();
                            })(i);
                          }
                        })();
                      }
                    }, 500);

                    var ad = false;
                    var ad__interval = setInterval(function () {

                      for (a = 0; a < tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map.length; a++) {
                        if (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[a]["state"] != "undefined" &&
                          (tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[a]["state"] == "marker" ||
                          tx_hivecptcntgooglemap_hivecptcntgooglemaprender__map[a]["state"] == "nomarker")) {
                          ad = true;
                        } else {
                          add = false;
                        }
                      }

                      if (ad) {
                        clearInterval(ad__interval);
                        console.warn("ad !!!");
                      }

                    }, 500);

                  } // map ready
                }
              }, 500); // maps interval
            }
          });
        });
      }
    }
  }
}, 500);

/**
 * TODO
 * get addresses for current markers
 * cause API changed
 */

// /**
//  * Get Map with coordinates as JSON
//  */
// var sPathAddress = '/index.php?id=' + sApiPid + '&token=' + sApiToken + '&type=' + sApiType +
//     '&tx_hiveextapi_request[address]=' +
//     '&findby[coordinate]=' + oCoordinates[ii]["uid"];
// if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
//     console.info('sPathAddress: ' + sPathAddress);
// }
// client.get(sPathAddress, function(response) {
//     obj = JSON.parse(response);
//     oAddress = obj;
//
//     if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
//         console.info(response);
//         console.info('oAddress: ' + typeof oAddress);
//         console.info(oAddress.length);
//         console.info(oAddress);
//     }
//
//     if (typeof oAddress == "object" && oAddress.length > 0) {
//         var sContent = '';
//         for (var a = 0; a < oAddress.length; a++) {
//             sContent +=
//                 '<address>' +
//                 '<strong>' +
//                 oAddress[a]["title"] +
//                 '</strong><br>' +
//                 oAddress[a]["street"] +
//                 ' ' +
//                 oAddress[a]["nr"] +
//                 '</address>'
//         }
//
//         if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
//             console.info('sContent: ' + sContent);
//         }
//
//         /**
//          * InfoWindow
//          */
//         aInfoWindow[ii] = new google.maps.InfoWindow({
//             content: sContent
//
//         });
//
//         if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
//             console.info(aInfoWindow[ii]);
//         }
//
//
//
//         /**
//          * Listener for InfoWindow
//          */
//         google.maps.event.addListener(aMarker[ii], 'click', (function(map, marker, infoWindow) {
//             return function() {
//                 infoWindow.open(map, marker);
//             }
//         })(aMap[i], aMarker[ii], aInfoWindow[ii]));
//     }
// });