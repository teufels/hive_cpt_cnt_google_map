<?php
namespace HIVE\HiveCptCntGoogleMap\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class MarkerIconTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMarkerIconReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMarkerIcon()
        );
    }

    /**
     * @test
     */
    public function setMarkerIconForStringSetsMarkerIcon()
    {
        $this->subject->setMarkerIcon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'markerIcon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHexReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHex()
        );
    }

    /**
     * @test
     */
    public function setHexForStringSetsHex()
    {
        $this->subject->setHex('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'hex',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getIcon()
        );
    }

    /**
     * @test
     */
    public function setIconForFileReferenceSetsIcon()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setIcon($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'icon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconSizeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIconSize()
        );
    }

    /**
     * @test
     */
    public function setIconSizeForStringSetsIconSize()
    {
        $this->subject->setIconSize('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'iconSize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconAnchorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIconAnchor()
        );
    }

    /**
     * @test
     */
    public function setIconAnchorForStringSetsIconAnchor()
    {
        $this->subject->setIconAnchor('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'iconAnchor',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCoordinateReturnsInitialValueForCoordinate()
    {
    }

    /**
     * @test
     */
    public function setCoordinateForCoordinateSetsCoordinate()
    {
    }
}
