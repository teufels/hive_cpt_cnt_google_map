<?php
namespace HIVE\HiveCptCntGoogleMap\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class MapTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Model\Map
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntGoogleMap\Domain\Model\Map();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRenderReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRender()
        );
    }

    /**
     * @test
     */
    public function setRenderForStringSetsRender()
    {
        $this->subject->setRender('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'render',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCoordinateReturnsInitialValueForCoordinate()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCoordinate()
        );
    }

    /**
     * @test
     */
    public function setCoordinateForObjectStorageContainingCoordinateSetsCoordinate()
    {
        $coordinate = new \HIVE\HiveExtAddress\Domain\Model\Coordinate();
        $objectStorageHoldingExactlyOneCoordinate = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCoordinate->attach($coordinate);
        $this->subject->setCoordinate($objectStorageHoldingExactlyOneCoordinate);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneCoordinate,
            'coordinate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addCoordinateToObjectStorageHoldingCoordinate()
    {
        $coordinate = new \HIVE\HiveExtAddress\Domain\Model\Coordinate();
        $coordinateObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $coordinateObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($coordinate));
        $this->inject($this->subject, 'coordinate', $coordinateObjectStorageMock);

        $this->subject->addCoordinate($coordinate);
    }

    /**
     * @test
     */
    public function removeCoordinateFromObjectStorageHoldingCoordinate()
    {
        $coordinate = new \HIVE\HiveExtAddress\Domain\Model\Coordinate();
        $coordinateObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $coordinateObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($coordinate));
        $this->inject($this->subject, 'coordinate', $coordinateObjectStorageMock);

        $this->subject->removeCoordinate($coordinate);
    }

    /**
     * @test
     */
    public function getCenterCoordinateReturnsInitialValueForCoordinate()
    {
    }

    /**
     * @test
     */
    public function setCenterCoordinateForCoordinateSetsCenterCoordinate()
    {
    }

    /**
     * @test
     */
    public function getMarkerIconReturnsInitialValueForMarkerIcon()
    {
        self::assertEquals(
            null,
            $this->subject->getMarkerIcon()
        );
    }

    /**
     * @test
     */
    public function setMarkerIconForMarkerIconSetsMarkerIcon()
    {
        $markerIconFixture = new \HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon();
        $this->subject->setMarkerIcon($markerIconFixture);

        self::assertAttributeEquals(
            $markerIconFixture,
            'markerIcon',
            $this->subject
        );
    }
}
