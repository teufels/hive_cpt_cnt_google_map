##########################################
## INCLUDE STAGING && PRODUCTION CONFIG ##
##########################################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_cpt_cnt_google_map/Configuration/TsConfig/Page/Production/config.txt">

################################
## INCLUDE DEVELOPMENT CONFIG ##
################################
[globalString = ENV:HTTP_HOST=development.*]
    <INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_cpt_cnt_google_map/Configuration/TsConfig/Page/Development/config.txt">
[global]