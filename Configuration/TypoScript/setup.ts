
plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_google_map/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_google_map/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_google_map/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_google_map/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_google_map/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_google_map/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptcntgooglemap._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-cnt-google-map table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-cnt-google-map table th {
        font-weight:bold;
    }

    .tx-hive-cpt-cnt-google-map table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hivecptcntgooglemap {
        model {
            HIVE\HiveCptCntGoogleMap\Domain\Model\Map {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntgooglemap.model.HIVE\HiveCptCntGoogleMap\Domain\Model\Map.persistence.storagePid}
                }
            }
        }
    }
}
plugin {
    tx_hivecptcntgooglemap_hivecptcntgooglemaprender {
        model {
            HIVE\HiveCptCntGoogleMap\Domain\Model\Map {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntgooglemap.model.HIVE\HiveCptCntGoogleMap\Domain\Model\Map.persistence.storagePid}
                }
            }
            HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntgooglemap.model.HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon.persistence.storagePid}
                }
            }
            HIVE\HiveCptCntGoogleMap\Domain\Model\InfoWindow {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntgooglemap.model.HIVE\HiveCptCntGoogleMap\Domain\Model\InfoWindow.persistence.storagePid}
                }
            }
        }
        settings {
            map {
                js {
                    options = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.settings.map.js.options}
                    styles = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.settings.map.js.styles}
                }
            }
            fusionTablesLayer {
                tableDocId = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.settings.fusionTablesLayer.tableDocId}
                tabelSelectField = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.settings.fusionTablesLayer.tabelSelectField}
                js {
                    styles = {$plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender.settings.fusionTablesLayer.js.styles}
                }
            }
        }
    }
}

##
## Extend hive_ext_api
##

##
## Plugin
## API Request for map
##
plugin {
    tx_hivecptcntgooglemap_hivecptcntgooglemaprequest {
        settings {
            api {
                request {
                    pid = {$plugin.tx_hiveextapi_request.settings.api.request.pid}
                    token = {$plugin.tx_hiveextapi_request.settings.api.request.token}
                    typeNum = {$plugin.tx_hiveextapi_request.settings.api.request.typeNum}
                }
            }
        }
    }
}

##
## Lib
## API Request for map
##
lib.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest = COA
lib.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = HiveCptCntGoogleMap
		pluginName = Hivecptcntgooglemaprequest
		vendorName = HIVE
		controller = Request
		action = request
		settings =< plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.settings
		persistence =< plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.persistence
		view =< plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest.view
	}
}

##
## PAGE for API
##
[PIDinRootline = {$plugin.tx_hiveextapi_request.settings.api.request.pid}] && [globalVar = GP:token={$plugin.tx_hiveextapi_request.settings.api.request.token}] && [globalVar = GP:ext=hive_cpt_cnt_google_map]

api_tx_hiveextapi_request.10 < lib.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest

[global]