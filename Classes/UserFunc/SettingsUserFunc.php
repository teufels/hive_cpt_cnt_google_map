<?php
namespace HIVE\HiveCptCntGoogleMap\UserFunc;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class SettingsUserFunc
{
    /**
     * @param $sPlugin
     * @return mixed
     */
    public static function getTypoScriptSettingsForPlugin ($sPlugin) {
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $aTypoScriptSettingsForPlugin = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.'];
        return $aTypoScriptSettingsForPlugin;
    }

}