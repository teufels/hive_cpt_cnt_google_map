<?php
namespace HIVE\HiveCptCntGoogleMap\Controller;

use HIVE\HiveExtApi\Mvc\View\JsonView;
/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * RequestController
 */
class RequestController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * requestRepository
     *
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Repository\RequestRepository
     * @inject
     */
    protected $requestRepository = null;

    /**
     * requestService
     *
     * @var \HIVE\HiveExtApi\Service\RequestService
     * @inject
     */
    protected $requestService = null;

    /**
     * @var \HIVE\HiveExtApi\Mvc\View\JsonView
     */
    protected $view = null;

    /**
     * @var string
     */
    protected $defaultViewObjectName = 'HIVE\\HiveExtApi\\Mvc\\View\\JsonView';

    /**
     * action request
     *
     * @return void
     */
    public function requestAction()
    {
        $aRequestArguments = $this->request->getArguments();
        $aGetFindBy = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('findby');
        $sRequestMethod = $this->request->getMethod();

        /*
         * Response:
         *
         * [
         *      key => "map-api",
         *      status => [200, ""],
         *      response => []
         * ]
         *
         */

        $aResponse = $this->requestService->handleRequest("HIVE\\HiveCptCntGoogleMap", $sRequestMethod, $aRequestArguments, $aGetFindBy);

        $this->view->setConfiguration(
            [
                'map-api' => [
                    '_descendAll' => [
                        '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                        '_descend' => [
                            'coordinate' => [
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                '_descend' => [
                                ]
                            ],
                            'centerCoordinate' => [
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                '_descend' => [
                                ]
                            ],
                            'markerIcon' => [
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                '_descend' => [
                                    'icon' => [
                                        '_only' => ["uid", "pid", "originalResource"],
                                        '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                        '_descend' =>[
                                            'originalResource' => [
                                                '_only' => ['publicUrl'],
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ]
                    ]
                ],
                'marker_icon-api' => [
                    '_descendAll' => [
                        '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                        '_descend' => [
                            'coordinate' => [
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                            ],
                            'icon' => [
                                '_only' => ["uid", "pid", "originalResource"],
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                '_descend' =>[
                                    'originalResource' => [
                                        '_only' => ['publicUrl'],
                                        '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                    ],
                                ],
                            ],
                        ]
                    ]
                ],
                'info_window-api' => [
                    '_descendAll' => [
                        '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                        '_descend' => [
                            'coordinate' => [
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                            ],
                            'image' => [
                                '_only' => ["uid", "pid", "originalResource"],
                                '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                '_descend' =>[
                                    'originalResource' => [
                                        '_only' => ['publicUrl'],
                                        '_exposeClassName' => JsonView::EXPOSE_CLASSNAME_FULLY_QUALIFIED,
                                    ],
                                ],
                            ],
                        ]
                    ]
                ],
            ]
        );

        if ($aResponse['status'][0] != 200) $this->response->setStatus($aResponse['status'][0], $aResponse['status'][1]);

        $this->view->setVariablesToRender([$aResponse['key']]);
        $this->view->assign($aResponse['key'], $aResponse['response']);
        return $this->view->render();

    }
}
