<?php
namespace HIVE\HiveCptCntGoogleMap\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * MarkerIcon
 */
class MarkerIcon extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * markerIcon
     *
     * @var string
     */
    protected $markerIcon = '';

    /**
     * hex
     *
     * @var string
     */
    protected $hex = '';

    /**
     * icon
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $icon = null;

    /**
     * iconSize
     *
     * @var string
     */
    protected $iconSize = '';

    /**
     * iconAnchor
     *
     * @var string
     */
    protected $iconAnchor = '';

    /**
     * coordinate
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\Coordinate
     */
    protected $coordinate = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the icon
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets the icon
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $icon
     * @return void
     */
    public function setIcon(\TYPO3\CMS\Extbase\Domain\Model\FileReference $icon)
    {
        $this->icon = $icon;
    }

    /**
     * Returns the coordinate
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Sets the coordinate
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate
     * @return void
     */
    public function setCoordinate(\HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate)
    {
        $this->coordinate = $coordinate;
    }

    /**
     * Returns the markerIcon
     *
     * @return string $markerIcon
     */
    public function getMarkerIcon()
    {
        return $this->markerIcon;
    }

    /**
     * Sets the markerIcon
     *
     * @param string $markerIcon
     * @return void
     */
    public function setMarkerIcon($markerIcon)
    {
        $this->markerIcon = $markerIcon;
    }

    /**
     * Returns the hex
     *
     * @return string $hex
     */
    public function getHex()
    {
        return $this->hex;
    }

    /**
     * Sets the hex
     *
     * @param string $hex
     * @return void
     */
    public function setHex($hex)
    {
        $this->hex = $hex;
    }

    /**
     * Returns the iconSize
     *
     * @return string $iconSize
     */
    public function getIconSize()
    {
        return $this->iconSize;
    }

    /**
     * Sets the iconSize
     *
     * @param string $iconSize
     * @return void
     */
    public function setIconSize($iconSize)
    {
        $this->iconSize = $iconSize;
    }

    /**
     * Returns the iconAnchor
     *
     * @return string $iconAnchor
     */
    public function getIconAnchor()
    {
        return $this->iconAnchor;
    }

    /**
     * Sets the iconAnchor
     *
     * @param string $iconAnchor
     * @return void
     */
    public function setIconAnchor($iconAnchor)
    {
        $this->iconAnchor = $iconAnchor;
    }
}
