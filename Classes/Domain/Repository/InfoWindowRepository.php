<?php
namespace HIVE\HiveCptCntGoogleMap\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for InfoWindows
 */
class InfoWindowRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveCptCntGoogleMap\\Domain\\Model\\InfoWindow';
        $sUserFuncPlugin = 'tx_hivecptcntgooglemap';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
