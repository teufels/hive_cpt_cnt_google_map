<?php
namespace HIVE\HiveCptCntGoogleMap\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use HIVE\HiveCptCntGoogleMap\UserFunc\SettingsUserFunc;
/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Class CptCntGoogleMapViewHelper
 * @package HIVE\HiveCptCntGoogleMap\ViewHelpers
 */
class CptCntGoogleMapViewHelper extends AbstractTagBasedViewHelper
{

    /**
     * mapRepository
     *
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Repository\MapRepository
     * @inject
     */
    protected $mapRepository = null;

    /**
     * render
     *
     * Namespace:
     * <div xmlns:hive="http://typo3.org/ns/HIVE/HiveCptCntGoogleMap/ViewHelpers">
     *
     * Use in Fluid:
     * <hive:cptCntGoogleMap uid="i" pluginUid="ii"/>
     *
     * Important:
     * pluginUid must be unique on page!
     *
     * @param int $uid
     * @param int $pluginUid
     * @return array|string
     */
    public function render($uid, $pluginUid) {

        $aSettings = [];

        $aSettings['pluginUid'] = $pluginUid;

        $aSettings['api'] = [];
        $tx_hiveextapi_request = SettingsUserFunc::getTypoScriptSettingsForPlugin('tx_hiveextapi_request');
        if (is_array($tx_hiveextapi_request) and array_key_exists('settings.', $tx_hiveextapi_request)) {
            if (is_array($tx_hiveextapi_request['settings.']) and array_key_exists('api.', $tx_hiveextapi_request['settings.'])) {
                if (is_array($tx_hiveextapi_request['settings.']['api.']) and array_key_exists('request.', $tx_hiveextapi_request['settings.']['api.'])) {
                    if (array_key_exists('pid', $tx_hiveextapi_request['settings.']['api.']['request.'])) {
                        $aSettings['api']['pid'] = $tx_hiveextapi_request['settings.']['api.']['request.']['pid'];
                    }
                    if (array_key_exists('token', $tx_hiveextapi_request['settings.']['api.']['request.'])) {
                        $aSettings['api']['token'] = $tx_hiveextapi_request['settings.']['api.']['request.']['token'];
                    }
                    if (array_key_exists('typeNum', $tx_hiveextapi_request['settings.']['api.']['request.'])) {
                        $aSettings['api']['typeNum'] = $tx_hiveextapi_request['settings.']['api.']['request.']['typeNum'];
                    }
                }
            }
        }

        $aSettings['map'] = [];
        $aSettings['map']['uid'] = $uid;
        $tx_hivecptcntgooglemap_hivecptcntgooglemaprender = SettingsUserFunc::getTypoScriptSettingsForPlugin('tx_hivecptcntgooglemap_hivecptcntgooglemaprender');
        if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender) and
            array_key_exists('settings.', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender)) {
            if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']) and
                array_key_exists('map.', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.'])) {
                if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['map.']) and
                    array_key_exists('js.', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['map.'])) {
                    if (array_key_exists('options', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['map.']['js.'])) {
                        $aSettings['map']['js']['otions'] = $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['map.']['js.']['options'];
                    }
                    if (array_key_exists('styles', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['map.']['js.'])) {
                        $aSettings['map']['js']['styles'] = $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['map.']['js.']['styles'];
                    }
                }

            }
        }

        $aSettings['fusionTablesLayer'] = [];
        $aSettings['fusionTablesLayer']['uid'] = $uid;
        if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender) and
            array_key_exists('settings.', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender)) {
            if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']) and
                array_key_exists('fusionTablesLayer.', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.'])) {
                if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.']) and
                    array_key_exists('js.', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.'])) {
                    if (array_key_exists('styles', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.']['js.'])) {
                        $aSettings['fusionTablesLayer']['js']['styles'] = $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.']['js.']['styles'];
                    }
                }
                if (is_array($tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.'])) {
                    if (array_key_exists('tableDocId', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.'])) {
                        $aSettings['fusionTablesLayer']['tableDocId'] = $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.']['tableDocId'];
                    }
                    if (array_key_exists('tabelSelectField', $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.'])) {
                        $aSettings['fusionTablesLayer']['tabelSelectField'] = $tx_hivecptcntgooglemap_hivecptcntgooglemaprender['settings.']['fusionTablesLayer.']['tabelSelectField'];
                    }
                }
            }
        }

        if (!empty($uid) and !empty($pluginUid)) {
            return $this->returnHtml($aSettings);
        }

        return '
        <div class="alert alert-danger" role="alert">
          <strong>Oh snap!</strong> uid or pluginUid empty!
        </div>';

    }

    private function returnHtml ($aSettings = []) {

        $sHtml = '
            <div class="tx-hive-cpt-cnt-google-map-viewhelper">
                <div class="tx-hive-cpt-cnt-google-map-viewhelper__render">
                    <section class="hivecptcntgooglemap"
                             id="hivecptcntgooglemap__' . $aSettings['pluginUid'] . '"
                             data-options="' . $aSettings['map']['js']['otions'] . '"
                             data-styles="' . $aSettings['map']['js']['styles'] . '"
                             data-api-pid="' .$aSettings['api']['pid'] . '"
                             data-api-map-id="' . $aSettings['map']['uid'] . '"
                             data-api-token="' . $aSettings['api']['token'] . '"
                             data-api-type="' . $aSettings['api']['typeNum'] . '"
                             data-fusion-docID="' . $aSettings['fusionTablesLayer']['tableDocId'] . '"
                             data-fusion-tabelSelectField="' . $aSettings['fusionTablesLayer']['tabelSelectField'] . '"
                             data-fusion-styles="' . $aSettings['fusionTablesLayer']['js']['styles'] . '">
                    </section>
                </div>
            </div>
            ';

        return $sHtml;
    }


}
