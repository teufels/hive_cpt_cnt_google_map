<?php
namespace HIVE\HiveCptCntGooglemap\Hooks;
/**
 * This file is part of the "news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
/**
 * AutoConfiguration-Hook for RealURL
 *
 */
class RealUrlAutoConfiguration
{
    /**
     * Generates additional RealURL configuration and merges it with provided configuration
     *
     * @param       array $params Default configuration
     * @return      array Updated configuration
     */
    public function addConfig($params)
    {
        $sRequestPlugin = "tx_hivecptcntgooglemap_hivecptcntgooglemaprequest";
        return array_merge_recursive($params['config'], [
                'postVarSets' => [
                    '_DEFAULT' => [
                        'map-api' => [
                            [
                                'GETvar' => $sRequestPlugin . '[map-api]',
                                'lookUpTable' => [
                                    'table'       => 'tx_hivecptcntgooglemap_domain_model_map',
                                    'id_field'    => 'uid',
                                    'alias_field' => 'uid'
                                ],
                                'optional'    => true,
                            ],
                        ],
                        'marker_icon-api' => [
                            [
                                'GETvar' => $sRequestPlugin . '[marker_icon-api]',
                                'lookUpTable' => [
                                    'table'       => 'tx_hivecptcntgooglemap_domain_model_markericon',
                                    'id_field'    => 'uid',
                                    'alias_field' => 'uid'
                                ],
                                'optional'    => true,
                            ],
                        ],
                        'info_window-api' => [
                            [
                                'GETvar' => $sRequestPlugin . '[info_window-api]',
                                'lookUpTable' => [
                                    'table'       => 'tx_hivecptcntgooglemap_domain_model_infowindow',
                                    'id_field'    => 'uid',
                                    'alias_field' => 'uid'
                                ],
                                'optional'    => true,
                            ],
                        ],
                    ]
                ]
            ]
        );
    }
}